import React, {ReactElement} from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ViewStyle,
} from 'react-native';
import {BORDER_RADIUS} from 'src/styles/main';

export interface IRadioRowProps {
  children: ReactElement[];
}

export function RadioRow(props: IRadioRowProps) {
  const {children} = props;
  const n = children.length;
  return (
    <View style={styles.radioRow}>
      {React.Children.map(children, (child, i) => {
        const style = [
          child.props.style,
          i === n - 1 && styles.radioButtonLast,
        ];

        return React.cloneElement(child, {style});
      })}
    </View>
  );
}

export interface IRadioButtonProps {
  style?: StyleProp<ViewStyle>;
  label: string;
  onPress?: (event: GestureResponderEvent) => void;
}
export function RadioButton(props: IRadioButtonProps) {
  const {label, style, onPress} = props;
  return (
    <TouchableHighlight style={[styles.radioButton, style]} onPress={onPress}>
      <Text style={styles.radioButtonLabel}>{label}</Text>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  radioRow: {
    display: 'flex',
    flexDirection: 'row',
    borderRadius: BORDER_RADIUS,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'grey',
    overflow: 'hidden',
  },
  radioButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 50,
    borderRightWidth: 1,
    borderStyle: 'solid',
  },
  radioButtonLast: {
    borderRightWidth: 0,
  },
  radioButtonLabel: {
    textAlign: 'center',
  },
});
