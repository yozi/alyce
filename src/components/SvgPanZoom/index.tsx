import React, {PureComponent, ReactNode} from 'react';
import {Animated, FlexStyle, RegisteredStyle} from 'react-native';
import {
  PinchGestureHandler,
  PinchGestureHandlerStateChangeEvent,
  State,
} from 'react-native-gesture-handler';
import Svg, {Circle, G} from 'react-native-svg';
import {identity, Matrix, toSVG} from 'transformation-matrix';
import {zoom, zoomOnViewerCenter} from './features/zoom';
import {PointObject} from './types';

export interface ISvgPanZoomProps {
  children: ReactNode;
  viewBoxWidth: number;
  viewBoxHeight: number;
  width: number;
  height: number;
  style?: RegisteredStyle<FlexStyle>;
}

export interface ISvgPanZoomState {
  matrix: Matrix;
}

const USE_NATIVE_DRIVER = true; // TODO
const AnimatedG = Animated.createAnimatedComponent(G);
export default class SvgPanZoom extends PureComponent<
  ISvgPanZoomProps,
  ISvgPanZoomState
> {
  constructor(props: ISvgPanZoomProps) {
    super(props);
    this.state = {
      matrix: identity(),
    };
  }
  public render() {
    const {
      children,
      viewBoxWidth,
      viewBoxHeight,
      style,
      width,
      height,
    } = this.props;

    const {matrix} = this.state;

    // TODO: translation animation
    // TODO: conversion viewbox to dimensions

    return (
      <PinchGestureHandler
        onGestureEvent={this.onPinchGestureEvent}
        onHandlerStateChange={this.onPinchHandlerStateChange}>
        <Animated.View
          // style={{transform: [{scale: this.scale}]}}
          collapsable={false}>
          <Svg viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}>
            <AnimatedG
              // scale={this.scale}
              transform={{scale: this.scale}}>
              {children}
            </AnimatedG>
            <Circle
              cx="50"
              cy="50"
              r="5"
              stroke="blue"
              strokeWidth="2.5"
              fill="red"
            />
          </Svg>
        </Animated.View>
      </PinchGestureHandler>
    );
  }

  public zoomOnViewerCenter(scaleFactor: number): void {
    const {viewBoxWidth: width, viewBoxHeight: height} = this.props;

    this.setState(({matrix}) => {
      const newMatrix = zoomOnViewerCenter(
        matrix,
        {width, height},
        scaleFactor,
      );
      return {
        matrix: newMatrix,
      };
    });
  }

  public zoom(SVGPoint: PointObject, scaleFactor: number): void {
    this.setState(({matrix}) => {
      const newMatrix = zoom(matrix, SVGPoint, scaleFactor);
      return {
        matrix: newMatrix,
      };
    });
  }

  private readonly baseScale = new Animated.Value(1);
  private readonly pinchScale = new Animated.Value(1);
  private readonly scale = Animated.multiply(this.baseScale, this.pinchScale);
  private lastScale = 1;
  private readonly onPinchGestureEvent = Animated.event(
    [{nativeEvent: {scale: this.pinchScale}}],
    {useNativeDriver: USE_NATIVE_DRIVER},
  );

  private readonly onPinchHandlerStateChange = (
    event: PinchGestureHandlerStateChangeEvent,
  ) => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      this.lastScale *= event.nativeEvent.scale;
      console.debug(this.lastScale);

      this.baseScale.setValue(this.lastScale);
      this.pinchScale.setValue(1);
    }
  };
}
