import {applyToPoint, inverse, Matrix} from 'transformation-matrix';
import {PointObject} from '../types';

export function getSVGPoint(matrix: Matrix, point: PointObject): PointObject {
  const inverseMatrix = inverse(matrix);
  return applyToPoint(inverseMatrix, point);
}
