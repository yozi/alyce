import {Matrix, scale, transform, translate} from 'transformation-matrix';
import {getSVGPoint} from './common';
import {PointObject} from '../types';
export function zoom(
  matrix: Matrix,
  SVGPoint: PointObject,
  scaleFactor: number,
): Matrix {
  return transform(
    matrix,
    translate(SVGPoint.x, SVGPoint.y),
    scale(scaleFactor, scaleFactor),
    translate(-SVGPoint.x, -SVGPoint.y),
  );
}

export function zoomOnViewerCenter(
  matrix: Matrix,
  viewer: {width: number; height: number},
  scaleFactor: number,
): Matrix {
  const {width, height} = viewer;
  const SVGPoint = getSVGPoint(matrix, {x: width / 2, y: height / 2});

  return zoom(matrix, SVGPoint, scaleFactor);
}
