export type PointObject = Readonly<{x: number; y: number}>;
