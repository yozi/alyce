import React, {ReactNode} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {BORDER_RADIUS} from 'src/styles/main';

export enum CellLayout {
  Horizontal,
  Vertical,
}
interface IBigMenuCellProps {
  children: ReactNode;
  layout: CellLayout;
  onPress?(): void;
}
export default function BigMenuCell(props: IBigMenuCellProps) {
  const {children, onPress, layout} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.cell,
        layout === CellLayout.Horizontal && styles.horizontal,
        layout === CellLayout.Vertical && styles.vertical,
      ]}>
      {children}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cell: {
    backgroundColor: 'grey',
    borderRadius: BORDER_RADIUS,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    color: 'black',
    flex: 1,
  },
  vertical: {
    flexDirection: 'column',
  },
  horizontal: {
    flexDirection: 'row',
  },
});
