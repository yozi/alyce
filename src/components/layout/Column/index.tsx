import React, {ReactElement, useCallback, useMemo, useState} from 'react';
import {
  LayoutChangeEvent,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';

interface IProps {
  children: ReactElement | ReactElement[];
  gap: number;
  style?: StyleProp<ViewStyle>;
}

export default function Column(props: IProps) {
  const {children, gap, style} = props;
  const childrenCount = React.Children.count(children);
  const [{height}, setDimension] = useState({height: 0, width: 0});
  const onLayout = useCallback((event: LayoutChangeEvent) => {
    const layout = event.nativeEvent.layout;
    setDimension({height: layout.height, width: layout.width});
  }, []);
  const itemHeight = (height - gap * (childrenCount - 1)) / childrenCount;
  const marginStyles = useMemo(
    () =>
      StyleSheet.create({
        item: {
          height: itemHeight,
        },
        notLastItem: {
          marginBottom: gap,
        },
      }),
    [gap, itemHeight],
  );
  return (
    <View style={[styles.column, style]} onLayout={onLayout}>
      {React.Children.map(children, (child, index) => (
        <View
          key={index}
          style={[
            marginStyles.item,
            index !== childrenCount - 1 && marginStyles.notLastItem,
          ]}>
          {child}
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  column: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});
