export enum Screen {
  Home = 'Home',
  NewProject = 'NewProject',
  ChooseHoop = 'ChooseHoop',
  Canvas = 'Canvas',
  SvgExample = 'SvgExample',
}
