import {useCallback} from 'react';
import * as React from 'react';
import {StyleSheet, Text} from 'react-native';
import BigMenuCell, {CellLayout} from 'src/components/BigMenuCell';
import Column from 'src/components/layout/Column';
import {Screen} from 'src/screens/constants';
import {GAP} from 'src/styles/main';
import {useNavigation} from '@react-navigation/native';

export default function NewProjectScreen() {
  const navigation = useNavigation();
  const toChooseHoop = useCallback(() => {
    navigation.navigate(Screen.ChooseHoop);
  }, [navigation]);
  return (
    <Column gap={GAP} style={[styles.container, styles.column]}>
      <BigMenuCell layout={CellLayout.Vertical} onPress={toChooseHoop}>
        <Text style={styles.text}>Blank canvas</Text>
      </BigMenuCell>
      <BigMenuCell layout={CellLayout.Vertical}>
        <Text style={styles.text}>Express Design from Image</Text>
      </BigMenuCell>
      <BigMenuCell layout={CellLayout.Vertical}>
        <Text style={styles.text}>Draw with Stitches</Text>
      </BigMenuCell>
      <BigMenuCell layout={CellLayout.Vertical}>
        <Text style={styles.text}>Blank Cross Stitch</Text>
      </BigMenuCell>
    </Column>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: GAP,
  },
  text: {
    textAlign: 'center',
  },
  column: {
    flex: 1,
  },
});
