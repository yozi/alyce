import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Canvas from 'src/screens/CanvasScreen/Canvas';
import BottomControls from './BottomControls';
import TopControls from './TopControls';

export default function CanvasScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.canvasName}>Untitled Canvas*</Text>
      </View>
      <TopControls />
      <View style={styles.canvas}>
        <Canvas />
      </View>
      <BottomControls />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'blue',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#454545',
  },
  canvasName: {
    textAlign: 'center',
    color: 'white',
    padding: 5,
  },
  canvas: {
    flex: 1,
    display: 'flex',
  },
});
