import React, {useCallback, useRef, useState} from 'react';
import {
  LayoutChangeEvent,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Circle, Rect} from 'react-native-svg';
import Row from 'src/components/layout/Row';
import SvgPanZoom from 'src/components/SvgPanZoom';

interface IProps {}

export default function Canvas(props: IProps) {
  const [dimensions, setDimension] = useState({height: 0, width: 0});
  const onLayout = useCallback((event: LayoutChangeEvent) => {
    const layout = event.nativeEvent.layout;
    setDimension({height: layout.height, width: layout.width});
  }, []);

  const ref = useRef<SvgPanZoom>(null);
  const onMinus = useCallback(() => {
    const svgPanZoom = ref.current;
    if (svgPanZoom) {
      svgPanZoom.zoomOnViewerCenter(0.9);
    }
  }, []);
  const onPlus = useCallback(() => {
    const svgPanZoom = ref.current;
    if (svgPanZoom) {
      svgPanZoom.zoomOnViewerCenter(1.1);
    }
  }, []);
  return (
    <View style={styles.container} onLayout={onLayout}>
      <Row gap={0} style={styles.row}>
        <TouchableOpacity onPress={onMinus}>
          <Text style={styles.minus}>-</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPlus}>
          <Text style={styles.plus}>+</Text>
        </TouchableOpacity>
      </Row>

      <SvgPanZoom
        ref={ref}
        style={StyleSheet.absoluteFill}
        width={dimensions.width}
        height={dimensions.height}
        viewBoxHeight={100}
        viewBoxWidth={100}>
        <Circle
          cx="50"
          cy="50"
          r="45"
          stroke="blue"
          strokeWidth="2.5"
          fill="green"
        />
        <Rect
          x="0"
          y="0"
          width="100"
          height="100"
          stroke="red"
          strokeWidth="2"
          fill="yellow"
        />
      </SvgPanZoom>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  row: {
    backgroundColor: 'orange',
  },
  plus: {
    fontSize: 25,
    padding: 10,
  },
  minus: {
    fontSize: 25,
    padding: 10,
  },
});
