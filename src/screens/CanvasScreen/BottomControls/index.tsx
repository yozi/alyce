import React from 'react';
import {StyleSheet, View} from 'react-native';
import Button from 'src/screens/CanvasScreen/ControlButton';

export default function BottomControls() {
  return (
    <View style={styles.controls}>
      <Button>x</Button>
      <Button>x</Button>
      <Button>x</Button>
      <Button>x</Button>
      <Button>x</Button>
    </View>
  );
}

const styles = StyleSheet.create({
  controls: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#a7a7a7',
    justifyContent: 'space-between',
  },
});
