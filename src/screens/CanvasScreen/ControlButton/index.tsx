import React, {ReactNode} from 'react';

import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export interface IButtonProps {
  children: ReactNode;
}
export default function ControlButton(props: IButtonProps) {
  const {children} = props;
  return (
    <TouchableOpacity style={styles.button}>
      <Text style={styles.icon}>{children}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    padding: 10,
  },
  icon: {
    textAlign: 'center',
  },
});
