import {useNavigation} from '@react-navigation/native';
import React, {useCallback} from 'react';
import {StyleSheet, Text} from 'react-native';
import Row from 'src/components/layout/Row';
import BigMenuCell, {CellLayout} from 'src/components/BigMenuCell';
import Column from 'src/components/layout/Column';
import {Screen} from 'src/screens/constants';
import {GAP} from 'src/styles/main';

export default function HomeScreen() {
  const navigation = useNavigation();
  const onPress = useCallback(() => {
    navigation.navigate(Screen.NewProject);
  }, [navigation]);
  return (
    <Column style={styles.container} gap={GAP}>
      <Row gap={GAP} style={styles.row}>
        <BigMenuCell layout={CellLayout.Vertical} onPress={onPress}>
          <Text style={styles.text}>New project</Text>
        </BigMenuCell>
      </Row>
      <Row gap={GAP} style={styles.row}>
        <BigMenuCell layout={CellLayout.Vertical}>
          <Text style={styles.text}>Open project</Text>
        </BigMenuCell>
      </Row>
      <Row gap={GAP} style={styles.row}>
        <BigMenuCell layout={CellLayout.Vertical}>
          <Text style={styles.text}>Settings</Text>
        </BigMenuCell>
        <BigMenuCell layout={CellLayout.Vertical}>
          <Text style={styles.text}>Help</Text>
        </BigMenuCell>
      </Row>
    </Column>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
    margin: GAP,
  },
  text: {
    textAlign: 'center',
  },
  row: {
    flex: 1,
  },
});
