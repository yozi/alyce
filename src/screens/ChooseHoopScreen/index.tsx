import React, {useCallback, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Row from 'src/components/layout/Row';
import {RadioButton, RadioRow} from 'src/components/RadioRow';
import AutoHooops from 'src/screens/ChooseHoopScreen/AutoHoops';
import ManualHoops from 'src/screens/ChooseHoopScreen/ManualHoops';
import {Screen} from 'src/screens/constants';
import {BORDER_RADIUS, GAP} from 'src/styles/main';
import {useNavigation} from '@react-navigation/native';

enum Content {
  Auto,
  Manual,
}
export default function ChooseHoopScreen() {
  const navigation = useNavigation();
  const [content, setContent] = useState(Content.Auto);
  const chooseAuto = useCallback(() => setContent(Content.Auto), []);
  const chooseManual = useCallback(() => setContent(Content.Manual), []);
  const openCanvas = useCallback(() => navigation.navigate(Screen.Canvas), [
    navigation,
  ]);
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Choose a hoop:</Text>
      <RadioRow>
        <RadioButton label={'List'} onPress={chooseAuto} />
        <RadioButton label={'Custom'} onPress={chooseManual} />
      </RadioRow>
      <View style={styles.content}>
        {content === Content.Auto && <AutoHooops />}
        {content === Content.Manual && <ManualHoops />}
      </View>
      <Row gap={GAP} style={styles.buttons}>
        <TouchableOpacity style={styles.cancel}>
          <Text style={styles.label}>X</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.ok} onPress={openCanvas}>
          <Text style={styles.label}>L</Text>
        </TouchableOpacity>
      </Row>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: BORDER_RADIUS,
    borderColor: 'grey',
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
  },
  header: {
    textAlign: 'center',
  },
  content: {},
  cancel: {
    backgroundColor: 'grey',
    borderRadius: BORDER_RADIUS,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    color: 'white',
    height: 50,
  },
  ok: {
    backgroundColor: 'green',
    borderRadius: BORDER_RADIUS,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    color: 'white',
    height: 50,
  },
  label: {
    textAlign: 'center',
  },
  buttons: {
    marginTop: 20,
  },
});
