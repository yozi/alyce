import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {RadioButton, RadioRow} from 'src/components/RadioRow';

export default function AutoHooops() {
  return (
    <View style={styles.container}>
      <TextInput
        keyboardType={'numeric'}
        placeholder={'Manufacture'}
        style={styles.input}
      />
      <TextInput
        keyboardType={'numeric'}
        placeholder={'Machine'}
        style={styles.input}
      />
      <TextInput
        keyboardType={'numeric'}
        placeholder={'Hoop Size'}
        style={styles.input}
      />
      <Text style={styles.label}>Orientation</Text>
      <RadioRow>
        <RadioButton label={'Natural'} />
        <RadioButton label={'Rotated'} />
      </RadioRow>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  input: {
    borderStyle: 'solid',
    borderColor: 'grey',
    borderWidth: 1,
    height: 50,
    borderRadius: 25,
    marginTop: 10,
  },
  label: {
    textAlign: 'center',
    marginTop: 20,
  },
});
