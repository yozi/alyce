import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';

export default function ManualHoops() {
  return (
    <View style={styles.container}>
      <TextInput
        keyboardType={'numeric'}
        placeholder={'Width'}
        style={styles.input}
        defaultValue={'260'}
      />
      <TextInput
        keyboardType={'numeric'}
        placeholder={'Height'}
        style={styles.input}
        defaultValue={'360'}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  input: {
    borderStyle: 'solid',
    borderColor: 'grey',
    borderWidth: 1,
    height: 50,
    borderRadius: 25,
    marginTop: 10,
  },
});
