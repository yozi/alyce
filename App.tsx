import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {StatusBar} from 'react-native';
import CanvasScreen from 'src/screens/CanvasScreen';
import ChooseHoopScreen from 'src/screens/ChooseHoopScreen';
import {Screen} from 'src/screens/constants';
import HomeScreen from 'src/screens/HomeScreen';
import NewProjectScreen from 'src/screens/NewProjectScreen';

import SvgExample from './src/components/SvgExample';
const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />
      <Stack.Navigator initialRouteName={Screen.Home}>
        <Stack.Screen name={Screen.Home} component={HomeScreen} />
        <Stack.Screen
          name={Screen.NewProject}
          component={NewProjectScreen}
          options={{title: 'New Project'}}
        />
        <Stack.Screen
          name={Screen.SvgExample}
          component={SvgExample}
          options={{title: 'SvgExample'}}
        />
        <Stack.Screen
          name={Screen.ChooseHoop}
          component={ChooseHoopScreen}
          options={{title: 'Choose a hoop'}}
        />
        <Stack.Screen
          name={Screen.Canvas}
          component={CanvasScreen}
          options={{title: 'Canvas'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
